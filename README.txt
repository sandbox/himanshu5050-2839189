Objective:
1. Create an script to get uuids and tids of child site for given tid,uuid of master site.
2. Mapped all tids of child site with the correspoding master tid in Data Sync Table.
3. Run Test cases.

Environment Setup:
1. Setup two child sites : a.local.dd, b.local.dd --> Done
2. create 10 random taxonomies in both child sites, some of common taxonomies must occurs --------> Done
3. consolidate taxonomies of both site and import into master site. -------> Done




Steps to Map new taxonomies from Master to existing taxonomies at chid site.

1. create a module : Taxonomy Reverse Sync.
2. Create a table schema : master_data_sync
3. Create a function which can return and export entire taxonomy data of child site into CSV file.
4. cosolidate taxonomy data of both site, into a csv file.
5. use the csv file to map master tid to child tids and save them into a master_data_sync table.
6. Check and review the process.




MULTISITE DATA SYNC.


Inputs:
 - Taxonomy list on Master and all child sites.

 Challenges:
  - Get list of all sites in your code repository.
  - Collect taxonomies of all sites into a single data array. with there domain name,
  - Start loop on Master taxonomied and check if entry exits in data array or not.
  - if entry exists and mapped, then insert a row in master data sync table for that domain,
  - if not, do nothing. marked it as non -synced entry.




